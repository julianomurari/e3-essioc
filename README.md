
# e3-essioc  
ESS Site-specific EPICS module : essioc

## Description

This module will include the following modules in an IOC:
* autosave
* iocstats
* caputlog
* auth
* recsync

## Usage
The following lines should be added to an IOC startup script to load and
configure this module:

```
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")
```

## Environment variables
### Required to be set - no default values

* `IOCNAME` - IOC name

In e3, `IOCNAME` is used to define some PV names that include the e3 internal PVs, so 
`IOCNAME` environment variable must be set before the IOC starts. 

* `IOCDIR` - slug version of IOC name
* `LOG_SERVER_NAME` - log server host name or IP address

When the IOC is deployed manually, `IOCDIR` and `LOG_SERVER_NAME` can be set in the 
`st.cmd` startup script via epicsEnvSet command. 
When the IOC is deployed by the IOC deployment system, `IOCNAME`, `IOCDIR` and 
`LOG_SERVER_NAME` will be automatically set by the deployment system. 

### Recommended to be set for most IOCs - basic default values

* `AS_TOP` - autosave top level directory, default = `/tmp`
* `ASG_FILENAME` - access security definition filename, default = `unrestricted_access.acf`

The above two variables do have default values, but are good candidates for
overriding via the `st.cmd` file with the epicsEnvSet command.

### Override defaults only when necessary

* `LOGDISABLE` - set to 1 to disable logging, default = 0
* `FACNAME` - IOC logging facility name, empty string by default
* `ERRORLOG_SERVER_PORT` - default = 9001
* `CAPUTLOG_SERVER_PORT` - default = 9001

The above four variables can be set in the IOC's `st.cmd` start up script via epicsEnvSet 
command. They do have default values set in the startup script snippets that will be used 
if the environment variables are not defined.
