# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0]
### New Features
* Add documentation with usage information
### Bugfixes
* Pass IOCDIR to autosave snippet
### Other changes
* Updated auth from 0.1.0 to 0.2.0

## [0.4.1]
### New Features
### Bugfixes
### Other changes
Renamed from e3-common
