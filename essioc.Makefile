## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS


REQUIRED += autosave
ifneq ($(strip $(AUTOSAVE_DEP_VERSION)),)
autosave_VERSION=$(AUTOSAVE_DEP_VERSION)
endif

REQUIRED += caputlog
ifneq ($(strip $(CAPUTLOG_DEP_VERSION)),)
caputlog_VERSION=$(CAPUTLOG_DEP_VERSION)
endif

REQUIRED += auth
ifneq ($(strip $(AUTH_DEP_VERSION)),)
auth_VERSION=$(AUTH_DEP_VERSION)
endif

REQUIRED += iocstats
ifneq ($(strip $(IOCSTATS_DEP_VERSION)),)
iocstats_VERSION=$(IOCSTATS_DEP_VERSION)
endif

REQUIRED += recsync
ifneq ($(strip $(RECSYNC_DEP_VERSION)),)
recsync_VERSION=$(RECSYNC_DEP_VERSION)
endif


SCRIPTS += $(wildcard ../iocsh/*.iocsh)


.PHONY: db 
db: 

.PHONY: vlibs
vlibs:
